#!/usr/bin/env python3
# Import Stuff
import math
import operator
import itertools
from tabulate import tabulate
import csv

# Define constants
g = 9.81  # m/s^2

# Define Fuel tanks/engines
# This will be replaced with XML file in the future
fuel_tank_dict = {
    "Mk0_LF": {
        "radial_size": 0.625,
        "cost": 200,
        "mass": 250,
        "wet_mass": 225
    },
    "Mk1_LF": {
        "radial_size": 1.125,
        "cost": 550,
        "mass": 2250,
        "wet_mass": 2000
    },
    "Mk3_LF_long": {
        "radial_size": 3.75,
        "cost": 17200,
        "mass": 57140,
        "wet_mass": 50000
    },
    "Mk3_LF": {
        "radial_size": 3.75,
        "cost": 8600,
        "mass": 28570,
        "wet_mass": 25000
    }
}

engine_dict = {
    "LV-N": {
        "radial_size": 1.125,
        "cost": 10000,
        "mass": 3000,
        "isp_atm": 185,
        "isp_vac": 800,
        "thrust": 60000
    },
    "LV-N-B": {
        "radial_size": 1.125,
        "cost": 20000,
        "mass": 6000,
        "isp_atm": 225,
        "isp_vac": 1000,
        "thrust": 40000
    }
}


# Define useful general functions
def check_list(input_list, length):
    new_list = []
    if type(input_list) is not list:
        new_list = [input_list] * length
    elif len(input_list) is length:
        new_list = input_list
    elif len(input_list) < length:
        new_list = input_list + [input_list[-1]] * (length - len(input_list))
    elif len(input_list) > length:
        new_list = input_list[:length]
    return new_list


# Define stage object
class Stage:
    def __init__(self, tank_qty=None, engine_qty=None, symmetry=2):
        self.tank_qty = tank_qty
        self.engine_qty = engine_qty
        self.symmetry = symmetry

        # Initialize stage properties
        self.calc_mass()
        self.calc_thrust_and_isp()
        self.calc_part_count()

    # Calculate properties of stage
    def calc_mass(self):
        mass = 0
        wet_mass = 0
        for tank in self.tank_qty:
            mass += fuel_tank_dict[tank]["mass"] * self.tank_qty[tank] * self.symmetry
            wet_mass += fuel_tank_dict[tank]["wet_mass"] * self.tank_qty[tank] * self.symmetry
        for engine in self.engine_qty:
            mass += engine_dict[engine]["mass"] * self.engine_qty[engine] * self.symmetry
        dry_mass = mass - wet_mass

        self.mass, self.wet_mass, self.dry_mass = mass, wet_mass, dry_mass

    def calc_thrust_and_isp(self):
        weighted_vac_isp_sum = 0
        weighted_atm_isp_sum = 0
        thrust = 0
        for engine in self.engine_qty:
            weighted_vac_isp_sum += engine_dict[engine]["isp_vac"] * engine_dict[engine]["thrust"] * self.engine_qty[engine]
            weighted_atm_isp_sum += engine_dict[engine]["isp_atm"] * engine_dict[engine]["thrust"] * self.engine_qty[engine]
            thrust += engine_dict[engine]["thrust"] * self.engine_qty[engine]

        isp_vac = 0
        isp_atm = 0
        if thrust > 0:
            isp_vac = weighted_vac_isp_sum / thrust
            isp_atm = weighted_atm_isp_sum / thrust

        self.thrust, self.isp_vac, self.isp_atm = thrust * self.symmetry, isp_vac, isp_atm

    def calc_part_count(self):
        self.part_count = (sum(self.engine_qty.values()) + sum(self.tank_qty.values())) * self.symmetry

    # Add/Remove fuel tanks
    def add_tanks(self, tank_type, amount):
        if amount < 1:
            print("WARNING: Cannot add negative tanks. Modification will not be implemented.")
        else:
            self.tank_qty[tank_type] += amount
        self.calc_mass()
        self.calc_part_count()

    def remove_tanks(self, tank_type, amount):
        if tank_type not in self.tank_qty:
            print("WARNING: Tank type is not in tank list. Tank cannot be removed.")
        elif self.tank_qty[tank_type] - amount < 0:
            print("WARNING: Requested change would make tank count negative. Removing tank type from stage instead.")
            self.tank_qty.pop(tank_type)
        else:
            self.tank_qty[tank_type] -= amount

        self.calc_mass()
        self.calc_part_count()

    # Add/Remove engines
    def add_engines(self, engine_type, amount):
        if amount < 1:
            print("WARNING: Cannot add negative engines. Modification will not be implemented.")
        else:
            self.engine_qty[engine_type] += amount

        self.calc_mass()
        self.calc_thrust_and_isp()
        self.calc_part_count()

    def remove_engines(self, engine_type, amount):
        if engine_type not in self.engine_qty:
            print("WARNING: Engine type is not in tank list. Engine cannot be removed.")
        elif self.engine_qty[engine_type] - amount < 0:
            print("WARNING: Requested change would make engine count negative. "
                  "Removing engine type from stage instead.")
            self.engine_qty.pop(engine_type)
        else:
            self.engine_qty[engine_type] -= amount

        self.calc_mass()
        self.calc_thrust_and_isp()
        self.calc_part_count()


class TransferStage:
    def __init__(self, stages, payload_mass_sequence, is_detached_sequence, decoupler_mass=50):
        self.stage_list = stages
        self.decoupler_mass = decoupler_mass
        self.n_stages = len(stages)
        self.payload_mass_seq = check_list(payload_mass_sequence, self.n_stages)
        self.is_detached_seq = check_list(is_detached_sequence, self.n_stages)

        self.payload_mass_change_seq = [self.payload_mass_seq[0]]
        for i in range(1, len(self.payload_mass_seq)):
            self.payload_mass_change_seq.append(self.payload_mass_seq[i] - self.payload_mass_seq[i-1])

        # Initialize stage properties
        self.calc_mass_seq()
        self.calc_thrust_and_isp_seq()
        self.calc_part_count()
        self.calc_delta_v()

    # Calculate properties of transfer stage
    def calc_mass_seq(self):
        mass_sequence = [[0, 0]]
        for i in range(0, self.n_stages):
            mass_sequence.append([self.stage_list[i].mass, self.stage_list[i].dry_mass])
            decoupler_mass = self.decoupler_mass * self.stage_list[i].symmetry * self.is_detached_seq[i]
            mass_sequence[i+1][0] += mass_sequence[i][0] + self.payload_mass_change_seq[i] + decoupler_mass
            mass_sequence[i+1][1] += mass_sequence[i][0] + self.payload_mass_change_seq[i] + decoupler_mass

        del mass_sequence[0]

        for i in range(0, self.n_stages):
            for j in range(i+1, self.n_stages):
                if self.is_detached_seq[j] or j > self.n_stages:
                    break
                mass_sequence[i][0] += self.stage_list[j].dry_mass
                mass_sequence[i][1] += self.stage_list[j].dry_mass
        self.mass_seq, self.mass = mass_sequence, mass_sequence[-1][0]

    def calc_thrust_and_isp_seq(self):
        thrust_sequence = []
        thrust = 0
        for stage in self.stage_list:
            thrust += stage.thrust
            thrust_sequence.append(thrust)

        weighted_vac_isp_sum = 0
        weighted_atm_isp_sum = 0
        isp_vac_sequence = []
        isp_atm_sequence = []
        for i in range(self.n_stages):
            if thrust_sequence[i] is 0:
                isp_vac_sequence.append(0)
                isp_atm_sequence.append(0)
            else:
                weighted_vac_isp_sum += self.stage_list[i].thrust * self.stage_list[i].isp_vac
                weighted_atm_isp_sum += self.stage_list[i].thrust * self.stage_list[i].isp_atm
                isp_vac_sequence.append(weighted_vac_isp_sum / thrust_sequence[i])
                isp_atm_sequence.append(weighted_atm_isp_sum / thrust_sequence[i])

        self.thrust_seq, self.isp_vac_seq, self.isp_atm_seq = thrust_sequence, isp_vac_sequence, isp_atm_sequence

    def calc_part_count(self):
        engine_count = 0
        tank_count = 0
        decoupler_count = 0
        for n in range(self.n_stages):
            engine_count += sum(self.stage_list[n].engine_qty.values()) * self.stage_list[n].symmetry
            tank_count += sum(self.stage_list[n].tank_qty.values()) * self.stage_list[n].symmetry
            decoupler_count += self.stage_list[n].symmetry * self.is_detached_seq[n]

        part_count = engine_count + tank_count + decoupler_count
        self.part_count, self.engine_count, self.tank_count, self.decoupler_count = \
            part_count, engine_count, tank_count, decoupler_count

    def calc_delta_v(self):
        delta_v_seq = []
        for i in range(self.n_stages):
            delta_v_seq.append(g * self.isp_vac_seq[i] *
                               math.log(self.mass_seq[i][0] / self.mass_seq[i][1]))

        self.delta_v_seq, self.delta_v = delta_v_seq, sum(delta_v_seq)

    def print_breakdown(self):
        breakdown_table = []
        for n in range(self.n_stages):
            breakdown_table.append([n, sum(self.stage_list[n].tank_qty.values()),
                                   sum(self.stage_list[n].engine_qty.values())])
        print(tabulate(breakdown_table, headers=["Stage #", "# of tanks", "# of engines"]))


# Define important functions
def add_engines_for_min_accel(transfer_stage, engine_type_seq, min_accel_seq):
        engine_type_seq = check_list(engine_type_seq, transfer_stage.n_stages)
        min_accel_seq = check_list(min_accel_seq, transfer_stage.n_stages)
        for i in range(transfer_stage.n_stages):
            stage_accel = transfer_stage.thrust_seq[i] / transfer_stage.mass_seq[i][0]
            if not stage_accel > min_accel_seq[i]:
                accel_diff = min_accel_seq[i] - stage_accel
                engines_to_add = math.ceil((transfer_stage.mass_seq[i][0] * accel_diff) /
                                           ((engine_dict[engine_type_seq[i]]["thrust"] -
                                            engine_dict[engine_type_seq[i]]["mass"] * accel_diff)))
                transfer_stage.stage_list[i].add_engines(engine_type_seq[i], engines_to_add)
                transfer_stage.calc_mass_seq()
                transfer_stage.calc_thrust_and_isp_seq()
                transfer_stage.calc_part_count()


def build_equal_drop_tank_transfer_stage(payload_mass, engine_type, fuel_tank_type, min_accel, delta_v_req,
                                         n_drop_tanks, max_parts=500):
    # Initialize stage list
    stages = [Stage({fuel_tank_type: 1}, {engine_type: 0}, symmetry=1)]
    for n in range(n_drop_tanks):
        stages.append(Stage({fuel_tank_type: 1}, {engine_type: 0}))

    transfer_stage = TransferStage(stages, payload_mass, True)

    # Add 1 fuel tank to each stage until delta V requirement is met
    # Also, add engines until minimum acceleration is met
    while transfer_stage.delta_v < delta_v_req:
        for stage in transfer_stage.stage_list:
            stage.add_tanks(fuel_tank_type, 1)
        add_engines_for_min_accel(transfer_stage, engine_type, min_accel)
        transfer_stage.calc_mass_seq()
        transfer_stage.calc_thrust_and_isp_seq()
        transfer_stage.calc_part_count()
        transfer_stage.calc_delta_v()
        if transfer_stage.part_count > max_parts:
            break

    return transfer_stage


mass = 500000
increment = 50000
table = []
delta_v_req = 7800
TWR = 0.1
stages = 4
tank = "Mk3_LF_long"
engine = "LV-N"

while mass <= 10000000:
    t_stage = build_equal_drop_tank_transfer_stage(mass, engine, tank, TWR*g, delta_v_req, stages-1, 150)
    if t_stage.delta_v < delta_v_req:
        break
    payload_fraction = mass / t_stage.mass * 100
    part_to_payload_mass_fraction = mass / t_stage.part_count
    table.append([mass/1000, t_stage.mass/1000, payload_fraction, t_stage.tank_count, t_stage.engine_count,
                  t_stage.part_count, part_to_payload_mass_fraction/1000])
    mass += increment

print("STAGES: ", stages)
print("FUEL TANK: ", tank)
print("ENGINE: ", engine)
print("DELTA V (m/s): ", delta_v_req)
print("TWR: ", TWR)
print("BURN TIME (hours): ", delta_v_req / (TWR*g*3600))
print(tabulate(table, headers=["Payload Mass", "Total Mass", "Mass Fraction", "Tank Count", "Engine Count",
                               "Total Part Count", "Part Ratio"]))

with open("output.csv", 'w') as f:
    wr = csv.writer(f)
    wr.writerows(table)

test_stage = build_equal_drop_tank_transfer_stage(800000, engine, tank, TWR*g, delta_v_req, stages-1, 150)
test_stage.print_breakdown()
